﻿namespace Silvr.Localization
{
    using System;
    using UnityEngine;

    public abstract class LocalizedObject : ScriptableObject
    {
        [SerializeField]
        [HideInInspector]
        private string typeName;

        public Type Type
        {
            get { return Type.GetType(this.typeName); }
            protected set { this.typeName = value.AssemblyQualifiedName; }
        }

        public abstract Translation CreateTranslation();
    }

    public abstract class LocalizedObject<T> : LocalizedObject, ISerializationCallbackReceiver
    {
        public T GetTranslation()
        {
            return LanguageManager.CurrentLanguage.GetTranslation<T>(this);
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            this.Type = typeof(T);
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            this.Type = typeof(T);
        }
    }
}