﻿namespace Silvr.Localization
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using System;

    [CreateAssetMenu(menuName = "Localization/Language Template")]
    public class LanguageTemplate : ScriptableObject
    {
        [SerializeField]
        private List<LocalizedObject> keys;

        public List<LocalizedObject> Keys
        {
            get { return this.keys; }
        }
    }
}