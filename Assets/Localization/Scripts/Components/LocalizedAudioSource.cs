﻿namespace Silvr.Localization.Components
{
    using Silvr.Localization.Assets;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [RequireComponent(typeof(AudioSource))]
    [DisallowMultipleComponent]
    public class LocalizedAudioSource : MonoBehaviour
    {
        public LocalizedAudioClip Clip;
        public bool PlayOnChange = true;

        private AudioSource audioSource;

        private void Awake()
        {
            this.audioSource = this.GetComponent<AudioSource>();
        }

        private void OnEnable()
        {
            LanguageManager.LanguageChanged += LanguageManager_LanguageChanged;
            this.UpdateTranslation();
        }

        private void OnDisable()
        {
            LanguageManager.LanguageChanged -= LanguageManager_LanguageChanged;
        }

        private void LanguageManager_LanguageChanged(object sender, System.EventArgs e)
        {
            this.UpdateTranslation();
        }

        private void UpdateTranslation()
        {
            this.audioSource.clip = this.Clip.GetTranslation();
            if (this.PlayOnChange)
            {
                this.audioSource.Play();
            }
        }
    }
}