﻿namespace Silvr.Localization.Components
{
    using Silvr.Localization.Assets;
    using System;
    using UnityEngine;
    using UnityEngine.UI;

    [RequireComponent(typeof(Text))]
    [DisallowMultipleComponent]
    public class LocalizedText : MonoBehaviour
    {
        public LocalizedString Text;

        private void OnEnable()
        {
            LanguageManager.LanguageChanged += LanguageManager_LanguageChanged;
            this.UpdateTranslation();
        }

        private void OnDisable()
        {
            LanguageManager.LanguageChanged -= LanguageManager_LanguageChanged;
        }

        private void LanguageManager_LanguageChanged(object sender, System.EventArgs e)
        {
            this.UpdateTranslation();
        }

        private void UpdateTranslation()
        {
            this.GetComponent<Text>().text = this.Text.GetTranslation();
        }
    }
}