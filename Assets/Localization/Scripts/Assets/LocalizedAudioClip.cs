﻿namespace Silvr.Localization.Assets
{
    using System;
    using UnityEngine;

    [CreateAssetMenu(menuName = "Localization/Localized assets/Localized Audio Clip")]
    public class LocalizedAudioClip : LocalizedObject<AudioClip>
    {
        public override Translation CreateTranslation()
        {
            return new UnityObjectTranslation<AudioClip>();
        }
    }
}