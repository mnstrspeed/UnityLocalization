﻿namespace Silvr.Localization.Assets
{
    using System;
    using UnityEngine;

    [CreateAssetMenu(menuName = "Localization/Localized assets/Localized String")]
    public class LocalizedString : LocalizedObject<string>
    {
        public override Translation CreateTranslation()
        {
            return new StringTranslation();
        }
    }
}