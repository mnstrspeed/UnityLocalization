﻿namespace Silvr.Localization
{
    using System;
    using UnityEngine;

    [Serializable]
    public class UnityObjectTranslation<T> : Translation<T> where T : UnityEngine.Object
    {
        public override T Value
        {
            get { return this.ObjectReferenceValue as T; }
        }
    }
}