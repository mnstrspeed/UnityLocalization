﻿namespace Silvr.Localization
{
    using System;

    [Serializable]
    public class StringTranslation : Translation<string>
    {
        public override string Value
        {
            get { return this.StringValue; }
        }
    }
}