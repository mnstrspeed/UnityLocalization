﻿namespace Silvr.Localization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    [CreateAssetMenu(menuName = "Localization/Language")]
    public partial class Language : ScriptableObject, ISerializationCallbackReceiver
    {
        // Template dictates which fields show up in the translation 
        // dialog. Agreement on what translations a language should contain
        [SerializeField]
        private LanguageTemplate template;

        [SerializeField]
        private List<Translation> translations = new List<Translation>();

        public LanguageTemplate Template
        {
            get { return this.template; }
        }

        public T GetTranslation<T>(LocalizedObject key)
        {
            for (int i = 0; i < this.translations.Count; i++)
            {
                if (this.translations[i].Key == key)
                {
                    return (this.translations[i] as Translation<T>).Value;
                }
            }
            throw new UnityException(
                string.Format("No translation found for key {0} in {1}", key.name, this.name));
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        { }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            // Generic type information is lost when Unity serializes translations,
            // so we reconstruct translation instances based on key type
            this.translations = this.translations
                .Where(t => t.Key != null)
                .Select(t => t.Key.CreateTranslation().CopyFrom(t)).ToList();
        }

    }
}