﻿namespace Silvr.Localization
{
    using System;
    using System.Linq;
    using UnityEngine;

    [CreateAssetMenu(menuName = "Localization/Language Manager")]
    public class LanguageManager : ScriptableObject
    {
        private static LanguageManager instance;
        public static LanguageManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = Resources.Load<LanguageManager>("LanguageManager");
                    if (instance == null)
                        throw new UnityException("LanguageManager resource is missing");
                }
                return instance;
            }
        }

        public static event EventHandler<EventArgs> LanguageChanged;
        private const string DefaultLanguagePlayerPrefsKey = "LanguageManager.DefaultLanguage";

        [SerializeField]
        private Language currentLanguage;
        [SerializeField]
        private Language defaultLanguage;
        [SerializeField]
        private Language[] languages;

        public static Language CurrentLanguage
        {
            get { return LanguageManager.Instance.currentLanguage; }
            set
            {
                LanguageManager.Instance.currentLanguage = value;
                if (LanguageManager.LanguageChanged != null)
                    LanguageManager.LanguageChanged(LanguageManager.Instance, new EventArgs());
            }
        }

        public static Language[] Languages
        {
            get { return LanguageManager.instance.languages; }
        }

        public static Language DefaultLanguage
        {
            get
            {
                if (PlayerPrefs.HasKey(DefaultLanguagePlayerPrefsKey))
                {
                    var language = LanguageManager.FindLanguageByName(PlayerPrefs.GetString(DefaultLanguagePlayerPrefsKey));
                    if (language != null)
                        return language;
                }
                return LanguageManager.instance.defaultLanguage;
            }
            set
            {
                string languageName = value.name;
                PlayerPrefs.SetString(DefaultLanguagePlayerPrefsKey, languageName);
            }
        }

        public static Language FindLanguageByName(string languageName)
        {
            return Languages.FirstOrDefault(language => language.name == languageName);
        }

#if UNITY_EDITOR

        private void OnValidate()
        {
            if (LanguageManager.LanguageChanged != null)
                LanguageManager.LanguageChanged(this, new EventArgs());
        }

#endif
    }
}