﻿namespace Silvr.Localization
{
    using System;

    [Serializable]
    public class Translation
    {
        public LocalizedObject Key;
        public string StringValue;
        public UnityEngine.Object ObjectReferenceValue;

        public Translation CopyFrom(Translation b)
        {
            this.Key = b.Key;
            this.StringValue = b.StringValue;
            this.ObjectReferenceValue = b.ObjectReferenceValue;

            return this;
        }
    }

    [Serializable]
    public abstract class Translation<T> : Translation
    {
        public abstract T Value { get; }
    }
}