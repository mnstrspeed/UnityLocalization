namespace Silvr.Localization.Editor
{
    using System.Linq;
    using UnityEngine;
    using UnityEditor;
    using Silvr.Localization;

    [CustomEditor(typeof(LocalizedObject), editorForChildClasses: true)]
    public class LocalizedObjectEditor : Editor
    {
        private const int IconSize = 32;

        private System.Type GetGenericObjectType()
        {
            var type = this.serializedObject.targetObject.GetType();

            while (type != null && (!type.IsGenericType || type.GetGenericTypeDefinition() != typeof(LocalizedObject<>)))
                type = type.BaseType;

            if (type != null && type.IsGenericType)
            {
                return type.GetGenericArguments().FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        public override void OnInspectorGUI()
        {
            this.DoTypeDescriptionGUI();
            EditorGUILayout.Space();
            this.DoReferencesGUI();
        }

        private void DoTypeDescriptionGUI()
        {
            var labelWithIconStyle = new GUIStyle(EditorStyles.label);
            labelWithIconStyle.padding.left += IconSize + labelWithIconStyle.margin.left;
            labelWithIconStyle.alignment = TextAnchor.MiddleLeft;
            labelWithIconStyle.richText = true;

            string containerTypeName = this.serializedObject.targetObject.GetType().ToString();
            string valueTypeName = this.GetGenericObjectType().FullName;
            EditorGUILayout.LabelField(string.Format("{0}\n<color=grey>({1})</color>", containerTypeName, valueTypeName),
                labelWithIconStyle, GUILayout.Height(IconSize));

            var iconRect = GUILayoutUtility.GetLastRect();
            iconRect.width = IconSize;

            var icon = AssetDatabase.GetCachedIcon(AssetDatabase.GetAssetPath(this.serializedObject.targetObject));
            if (icon != null)
                GUI.DrawTexture(iconRect, icon);
        }

        private void DoReferencesGUI()
        {
            var key = this.serializedObject.targetObject as LocalizedObject;
            var templates = Resources.FindObjectsOfTypeAll(typeof(LanguageTemplate))
                .Where(template => (template as LanguageTemplate).Keys.Contains(key)).ToArray();

            EditorGUILayout.LabelField("Used in", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            EditorGUI.BeginDisabledGroup(true);

            for (int i = 0; i < templates.Length; i++)
            {
                EditorGUILayout.ObjectField(GUIContent.none, templates[i], typeof(LanguageTemplate), false);
            }

            EditorGUI.EndDisabledGroup();
            EditorGUI.indentLevel--;
        }
    }
}