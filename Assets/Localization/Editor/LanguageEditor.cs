namespace Silvr.Localization.Editor
{
    using UnityEngine;
    using UnityEditor;
    using Silvr.Localization;
    using System.Collections.Generic;

    [CustomEditor(typeof(Language))]
    public class LanguageEditor : Editor
    {
        private Dictionary<LocalizedObject, SerializedProperty> cachedKeyTranslationProperties;

        private void OnEnable()
        {
            this.cachedKeyTranslationProperties = new Dictionary<LocalizedObject, SerializedProperty>();
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            var translations = serializedObject.FindProperty("translations");
            var templateProperty = serializedObject.FindProperty("template");
            var template = templateProperty.objectReferenceValue as LanguageTemplate;

            EditorGUILayout.PropertyField(templateProperty);
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Translations", EditorStyles.boldLabel);
            EditorGUILayout.BeginVertical(GUI.skin.box);

            if (template != null)
            {
                var keys = template.Keys;
                for (int i = 0; i < keys.Count; i++)
                {
                    if (keys[i] == null)
                        continue;

                    var property = this.FindOrCreateValuePropertyForKey(keys[i], translations);
                    EditorGUILayout.PropertyField(property);
                }
            }

            EditorGUILayout.EndVertical();

            serializedObject.ApplyModifiedProperties();
        }

        private SerializedProperty FindOrCreateValuePropertyForKey(LocalizedObject key, SerializedProperty translations)
        {
            if (this.cachedKeyTranslationProperties.ContainsKey(key))
            {
                return this.cachedKeyTranslationProperties[key];
            }
            else
            {
                var translationProperty = this.FindOrCreateValuePropertyForKeyUncached(key, translations);

                // Add to cache and return
                this.cachedKeyTranslationProperties.Add(key, translationProperty);
                return translationProperty;
            }
        }

        private SerializedProperty FindOrCreateValuePropertyForKeyUncached(LocalizedObject key, SerializedProperty translations)
        {
            SerializedProperty translationProperty;
            for (int i = 0; i < translations.arraySize; i++)
            {
                translationProperty = translations.GetArrayElementAtIndex(i);

                var t = translationProperty.FindPropertyRelative("Key").objectReferenceValue as LocalizedObject;
                if (t == key)
                {
                    return translationProperty;
                }
            }

            // Add new translation entry if not found
            translations.InsertArrayElementAtIndex(translations.arraySize);
            translationProperty = translations.GetArrayElementAtIndex(translations.arraySize - 1);
            translationProperty.FindPropertyRelative("Key").objectReferenceValue = key;
            // Reset value of new translation
            translationProperty.FindPropertyRelative("StringValue").stringValue = "";
            translationProperty.FindPropertyRelative("ObjectReferenceValue").objectReferenceValue = null;

            return translationProperty;
        }
    }
}