namespace Silvr.Localization.Editor
{
    using UnityEngine;
    using UnityEditor;
    using Silvr.Localization;

    [CustomPropertyDrawer(typeof(Translation))]
    public class TranslationPropertyDrawer : PropertyDrawer
    {
        private const float MaxHeight = 100f;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var key = property.FindPropertyRelative("Key").objectReferenceValue as LocalizedObject;

            if (key.Type == typeof(string))
            {
                var text = property.FindPropertyRelative("StringValue").stringValue;
                return Mathf.Min(MaxHeight, GUI.skin.textArea.CalcHeight(new GUIContent(text), float.PositiveInfinity));
            }
            else
            {
                return EditorGUIUtility.singleLineHeight;
            }
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var key = property.FindPropertyRelative("Key").objectReferenceValue as LocalizedObject;

            var contentRect = EditorGUI.PrefixLabel(position, new GUIContent(key.name), GUI.skin.label);
            var keyRect = new Rect(position.x, position.y, position.width - contentRect.width, position.height);

            // Ping key on label click
            var e = Event.current;
            if (e.type == EventType.MouseDown && keyRect.Contains(e.mousePosition))
            {
                EditorGUIUtility.PingObject(key);
                e.Use();
            }

            // Draw field
            if (key.Type == typeof(string))
            {
                var stringValueProperty = property.FindPropertyRelative("StringValue");
                stringValueProperty.stringValue = EditorGUI.TextArea(contentRect,
                    stringValueProperty.stringValue);
            }
            else if (typeof(UnityEngine.Object).IsAssignableFrom(key.Type))
            {
                EditorGUI.ObjectField(contentRect, property.FindPropertyRelative("ObjectReferenceValue"), key.Type, GUIContent.none);
            }
        }
    }
}