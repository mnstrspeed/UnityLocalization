namespace Silvr.Localization.Editor
{
    using UnityEngine;
    using UnityEditor;
    using System.Linq;
    using Silvr.Localization;
    using System;
    using UnityEditorInternal;

    [CustomEditor(typeof(LanguageTemplate))]
    public class LanguageTemplateEditor : Editor
    {
        private UnityEditorInternal.ReorderableList list;

        private void OnEnable()
        {
            this.list = new UnityEditorInternal.ReorderableList(this.serializedObject,
                this.serializedObject.FindProperty("keys"), true, true, true, true);
            this.list.drawHeaderCallback = this.DrawListHeader;
            this.list.drawElementCallback = this.DrawListElement;
            this.list.onAddCallback = this.OnAdd;
        }

        private void OnAdd(ReorderableList list)
        {
            int index = list.serializedProperty.arraySize;
            list.serializedProperty.arraySize++;
            // Reset object reference value
            list.serializedProperty.GetArrayElementAtIndex(index).objectReferenceValue = null;
        }

        private void DrawListHeader(Rect rect)
        {
            EditorGUI.LabelField(rect, "Keys");
        }

        private void DrawListElement(Rect rect, int index, bool isActive, bool isFocused)
        {
            // Apply padding
            rect.x += 1;
            rect.height -= 2;

            var element = this.list.serializedProperty.GetArrayElementAtIndex(index);
            EditorGUI.PropertyField(rect, element, GUIContent.none);
        }

        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();

            this.list.DoLayoutList();
            this.DoReferencesGUI();

            this.serializedObject.ApplyModifiedProperties();
        }

        private void DoReferencesGUI()
        {
            var template = this.serializedObject.targetObject as LanguageTemplate;
            var languages = Resources.FindObjectsOfTypeAll(typeof(Language))
                .Where(language => (language as Language).Template == template).ToArray();

            EditorGUILayout.LabelField("Implemented by", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            EditorGUI.BeginDisabledGroup(true);

            for (int i = 0; i < languages.Length; i++)
            {
                EditorGUILayout.ObjectField(GUIContent.none, languages[i], typeof(Language), false);
            }

            EditorGUI.EndDisabledGroup();
            EditorGUI.indentLevel--;
        }
    }
}