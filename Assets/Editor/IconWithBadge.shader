﻿Shader "Hidden/IconWithBadge"
{
	Properties
	{
		_MainTex ("Main icon", 2D) = "clear" {}
		_BadgeTex ("Badge icon", 2D) = "clear" {}
		_Color ("Color", Color) = (1,1,1,1)
		_BackgroundColor ("Background color", Color) = (0,0,0,0)
	}
	SubShader
	{
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 mainUv : TEXCOORD0;
				float2 badgeUv : TEXCOORD1;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.mainUv = v.uv;
				o.badgeUv = v.uv * 1.5 - 1.5 * float2(0.5 / 1.5, 0);
				return o;
			}
			
			uniform sampler2D _MainTex;
			uniform sampler2D _BadgeTex;
			uniform float4 _Color;
			uniform float4 _BackgroundColor;

			fixed4 clear_background(float2 uv)
			{
				float2 q = floor(uv * 4.);
				return lerp(1.0.xxxx, fixed4(0.75.xxx, 1), fmod(q.x + q.y, 2.));
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 main = tex2D(_MainTex, i.mainUv);
				fixed4 badge = tex2D(_BadgeTex, i.badgeUv);

				// Desaturate and tint main icon
				main.rgb = Luminance(main.rgb) * _Color.rgb;
				
				// Blend with badge icon
				fixed4 col = (1. - badge.a) * main + badge.a * badge;

				col = (1. - col.a) * _BackgroundColor + col.a * col;

				return col;
			}
			ENDCG
		}
	}
}
