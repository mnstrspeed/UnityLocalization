﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Linq;

public class CreateIconWithBadge : EditorWindow 
{
    private Texture mainIcon;
    private Texture badgeIcon;
    private Color color = new Color32(195, 212, 255, 255);

    private Material material;
    private Texture[] unityAssetIcons;
    private string unityAssetIconSearchText;

    [MenuItem("Tools/Create icon with badge")]
    public static void CreateWindow()
    {
        var window = (CreateIconWithBadge)EditorWindow.GetWindow<CreateIconWithBadge>();
        window.Show();
    }

    private void Awake()
    {
        this.material = new Material(Shader.Find("Hidden/IconWithBadge"));

        this.unityAssetIconSearchText = "";
        this.unityAssetIcons = AssetDatabase.LoadAllAssetsAtPath("Library/unity editor resources")
            .Where(a => a != null && a is Texture && a.name.EndsWith(" Icon"))
            .Select(a => a as Texture).ToArray();
    }

    public void OnGUI()
    {
        this.DoMainTextureSelectionGUI();

        //this.mainIcon = EditorGUILayout.ObjectField("Main Icon", this.mainIcon, typeof(Texture), false) as Texture;
        this.badgeIcon = EditorGUILayout.ObjectField("Badge Icon", this.badgeIcon, typeof(Texture), false) as Texture;
        this.color = EditorGUILayout.ColorField("Color", this.color);

        EditorGUILayout.BeginHorizontal(GUILayout.Width(64f + 32f + 16f + 2 * 4f    ));
        if (this.mainIcon != null && this.badgeIcon != null)
        {
            this.UpdateMaterial();

            var previewRect = GUILayoutUtility.GetRect(64f, 64f);
            EditorGUI.DrawPreviewTexture(previewRect, this.mainIcon, this.material, ScaleMode.ScaleToFit);

            previewRect = GUILayoutUtility.GetRect(32f, 32f);
            EditorGUI.DrawPreviewTexture(previewRect, this.mainIcon, this.material, ScaleMode.ScaleToFit);

            previewRect = GUILayoutUtility.GetRect(16f, 16f);
            EditorGUI.DrawPreviewTexture(previewRect, this.mainIcon, this.material, ScaleMode.ScaleToFit);
        }
        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("Generate", GUILayout.ExpandWidth(true), GUILayout.Height(32f)))
        {
            this.CreateIcon();
        }
    }

    private void DoMainTextureSelectionGUI()
    {
        this.unityAssetIconSearchText = EditorGUILayout.TextField(new GUIContent("Search asset icons"), this.unityAssetIconSearchText);

        var selected = this.unityAssetIcons.FirstOrDefault(a => a.name.StartsWith(this.unityAssetIconSearchText));
        if (selected != null)
            this.mainIcon = selected;
    }

    private void UpdateMaterial(bool includeBackground = true)
    {
        this.material.SetTexture("_BadgeTex", this.badgeIcon);
        this.material.SetColor("_Color", this.color);
        if (includeBackground)
        {
            this.material.SetColor("_BackgroundColor", EditorGUIUtility.isProSkin ?
                new Color(0.18f, 0.18f, 0.18f) : new Color(0.83f, 0.83f, 0.83f));
        }
    }

    private void CreateIcon()
    {
        RenderTexture temp = RenderTexture.GetTemporary(this.mainIcon.width, this.mainIcon.height, 0,
			RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);
            
        this.UpdateMaterial(false);
        this.material.SetColor("_BackgroundColor", Color.clear);

        Graphics.Blit(this.mainIcon, temp, material);

        RenderTexture prevRenderTexture = RenderTexture.active;
        RenderTexture.active = temp;

        Texture2D readableIcon = new Texture2D(temp.width, temp.height);
        readableIcon.ReadPixels(new Rect(0, 0, readableIcon.width, readableIcon.height), 0, 0);
        readableIcon.Apply();

        RenderTexture.active = prevRenderTexture;
        RenderTexture.ReleaseTemporary(temp);

        string path = EditorUtility.SaveFilePanelInProject("Save icon as PNG", this.mainIcon.name, "png", "Save icon as PNG");
        if (string.IsNullOrEmpty(path))
            return;
        File.WriteAllBytes(path, readableIcon.EncodeToPNG());

        var textureImporter = (TextureImporter)TextureImporter.GetAtPath(path);
        textureImporter.alphaIsTransparency = true;
        AssetDatabase.Refresh();
        AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
    }
}
