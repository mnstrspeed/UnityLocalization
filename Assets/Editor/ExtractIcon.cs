﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public static class ExtractIcon 
{
	[MenuItem("Tools/Extract selection icon")]
	public static void ExtractSelectionIcon()
	{
		string path = EditorUtility.SaveFolderPanel("Save icons as PNG", "", "Icons");
		if (string.IsNullOrEmpty(path))
			return;
			
		foreach (var obj in Selection.objects)
        {
            var all = AssetDatabase.LoadAllAssetsAtPath("Library/unity editor resources");
            
            EditorUtility.DisplayProgressBar("Saving icons", "Saving icons...", 0f);

			int i = 0;
			foreach (var a in all)
			{
				if (a != null && a is Texture && a.name.EndsWith(" Icon"))
				{
					Debug.Log(a.name);
					EditorUtility.DisplayProgressBar("Saving icons", "Saving " + a.name, i / (float)all.Length);

					SaveIcon(a as Texture, Path.Combine(path, a.name + ".png"));
				}
				i++;
			}

			EditorUtility.ClearProgressBar();

            //SaveIcon(icon);
        }

    }

    private static void SaveIcon(Texture icon, string path)
    {
        RenderTexture temp = RenderTexture.GetTemporary(icon.width, icon.height, 0,
			RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);

        Graphics.Blit(icon, temp);

        RenderTexture prevRenderTexture = RenderTexture.active;
        RenderTexture.active = temp;

        Texture2D readableIcon = new Texture2D(icon.width, icon.height);
        readableIcon.ReadPixels(new Rect(0, 0, readableIcon.width, readableIcon.height), 0, 0);
        readableIcon.Apply();

        RenderTexture.active = prevRenderTexture;
        RenderTexture.ReleaseTemporary(temp);

        File.WriteAllBytes(path, readableIcon.EncodeToPNG());
    }
}
